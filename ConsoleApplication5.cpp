

#include <iostream>



class Vector
{
public:
	Vector()
	{
		x = 0;
		y = 0;
		z = 0;
	}

	Vector(double _x, double _y, double _z) 
	{
		x = _x;
		y = _y;
		z = _z;
	}

	void Show()
	{
		std::cout<< "Vector: " << x << " " << y << " " << z << "\n";
	}

	void ShowVectorLength()
	{
		double length = sqrt(x*x + y*y + z*z);
		std::cout << "Vector length: " << length << "\n";
	}

private:
	double x, y, z;
};

int main()
{
	Vector vector(1, 2, 3);
	vector.Show();
	vector.ShowVectorLength();
}

